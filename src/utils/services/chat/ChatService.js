import axios from 'axios';
import { isAuthenticated } from '../auth/authService';

export const getAllChatService = async () => {
    const { token } = isAuthenticated();

    const res = await axios.get(`http://localhost:8080/chats`,
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}