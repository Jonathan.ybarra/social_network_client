import axios from 'axios';
import { isAuthenticated } from '../auth/authService';

export const createPostService = async ({ body, photo }) => {
    const { user, token } = isAuthenticated();

    const res = await axios.post(`http://localhost:8080/createpost/${user._id}`, {
        body,
        photo
    },
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}

export const getPostByService = async (id, page) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`http://localhost:8080/posts/by/${id}?page=${page}`,
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}

export const getPostUserMeService = async (id, page) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`http://localhost:8080/posts/users/me/${id}?page=${page}`,
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}

export const getPostIdService = async (postId) => {
    const { token } = isAuthenticated();
    const res = await axios.get(`http://localhost:8080/post/${postId}`,
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}


export const deletePostIdService = async (postId) => {
    const { token } = isAuthenticated();
    const res = await axios.delete(`http://localhost:8080/deletepost/${postId}`,
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}

export const likePostService = async (postId) => {
    const { token, user } = isAuthenticated();
    const res = await axios.put(`http://localhost:8080/post/like`, {
        userId: user._id,
        postId
    },
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}

export const unLikePostService = async (postId) => {
    const { token, user } = isAuthenticated();

    const res = await axios.put(`http://localhost:8080/post/unlike`, {
        userId: user._id,
        postId
    },
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}

export const commentPostService = async (postId, comment) => {
    const { token, user } = isAuthenticated();

    const res = await axios.put(`http://localhost:8080/post/comment`, {
        comment,
        postId,
        userId: user._id,
    },
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}

export const unCommentPostService = async (postId, comment) => {
    const { token } = isAuthenticated();

    const res = await axios.put(`http://localhost:8080/post/uncomment`, {
        comment,
        postId
    },
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}

export const updatePostService = async (postId, newPost) => {
    const { token, user } = isAuthenticated();
    console.log(token)
    const res = await axios.put(`http://localhost:8080/updatepost/${postId}`,
        { ...newPost },
        {
            headers:
            {
                Authorization: `Bearer ${token}`
            }
        }
    );
    return res;
}