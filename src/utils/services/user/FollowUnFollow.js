import axios from 'axios';
import { isAuthenticated } from '../auth/authService';

// FollowsServices
export const followUserService = async (userId, followId) => {
    const res = await axios.put(`http://localhost:8080/user/follow`, {
        userId,
        followId
    },
        {
            headers:
            {
                Authorization: `Bearer ${isAuthenticated().token}`
            }
        }
    );
    return res;
}

export const unfollowUserService = async (userId, unfollowId) => {
    const res = await axios.put(`http://localhost:8080/user/unfollow`, {
        userId,
        unfollowId
    },
        {
            headers:
            {
                Authorization: `Bearer ${isAuthenticated().token}`
            }
        }
    );
    return res;
}

export const deleteFollowerService = async (userId, unfollowId) => {
    const res = await axios.put(`http://localhost:8080/delete/follower`, {
        userId,
        unfollowId
    },
        {
            headers:
            {
                Authorization: `Bearer ${isAuthenticated().token}`
            }
        }
    );
    return res;
}