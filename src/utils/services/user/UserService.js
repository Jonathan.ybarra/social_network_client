import axios from 'axios';
import { isAuthenticated } from '../auth/authService';

export const userByIDService = async (id) => {
    const res = await axios.get(`http://localhost:8080/user/${id}`, {
        headers: {
            Authorization: `Bearer ${isAuthenticated().token}`
        }
    });
    return res;
}

export const userPhotoProfileService = async (id) => {
    const res = await axios.get(`http://localhost:8080/user/photo/${id}`, {
        headers: {
            Authorization: `Bearer ${isAuthenticated().token}`
        }
    });
    return res;
}

export const allUsersService = async () => {
    const res = await axios.get(`http://localhost:8080/users`, {
        headers: {
            Authorization: `Bearer ${isAuthenticated().token}`
        }
    });
    return res;
}

export const deleteUserService = async (id) => {
    const res = await axios.delete(`http://localhost:8080/user/${id}`, {
        headers: {
            Authorization: `Bearer ${isAuthenticated().token}`
        }
    });
    return res;
}

export const updateUserService = async (id, { name, email, link_profile_job, preference_one, preference_two, photo_profile, password, about }) => {
    const res = await axios.put(`http://localhost:8080/user/${id}`, {
        id,
        name,
        email,
        link_profile_job,
        preference_one,
        preference_two,
        photo_profile,
        password,
        about
    },
        {
            headers:
            {
                Authorization: `Bearer ${isAuthenticated().token}`
            }
        }
    );
    return res;
}

export const updateUserLocalStorageService = (user, next) => {
    if (typeof window !== "undefined") {
        if (localStorage.getItem('jwt')) {
            let auth = JSON.parse(localStorage.getItem('jwt'));
            auth.user = user;
            localStorage.setItem('jwt', JSON.stringify(auth));
            next();
        }
    }
}

export const findPeopleService = async (userId, unfollowId) => {
    const res = await axios.get(`http://localhost:8080/user/findpeople/${userId}`,
        {
            headers:
            {
                Authorization: `Bearer ${isAuthenticated().token}`
            }
        }
    );
    return res;
}

export const searchUserService = async (name) => {
    const res = await axios.get(`http://localhost:8080/searchusers`,
        {
            params: { name },
            headers:
            {
                Authorization: `Bearer ${isAuthenticated().token}`
            }
        }
    );
    return res;
}