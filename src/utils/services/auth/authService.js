import axios from 'axios';

export const signupService = async ({ name, email, password }) => {
    const res = await axios.post(`http://localhost:8080/auth/signup`, {
        name,
        email,
        password
    });
    return res;
}

export const signinService = async ({ email, password }) => {
    const res = await axios.post(`http://localhost:8080/auth/signin`, {
        email,
        password
    });
    return res;
}

export const signoutService = async () => {
    if (typeof window !== "undefined") localStorage.removeItem('jwt');
    const res = await axios.get(`http://localhost:8080/auth/signout`);
    return res;
}

export const isAuthenticated = () => {
    if (typeof window === "undefined") return false;

    if (localStorage.getItem('jwt')) {
        return JSON.parse(localStorage.getItem('jwt'));
    } else {
        return false;
    }
}

export const authenticateService = (jwt, next) => {
    if (typeof window !== "undefined") {
        localStorage.setItem('jwt', JSON.stringify(jwt));
        next();
    }
}

export const setUserAuthenticateLocalStorage = ({ photo_profile }) => {
    if (typeof window !== "undefined") {
        const { user, token } = JSON.parse(localStorage.getItem('jwt'));
        const newUser = { ...user, photo_profile }
        localStorage.setItem('jwt', JSON.stringify({ token, user: newUser }));
        return { ...newUser }
    }
}


export const socialLoginService = async user => {
    const res = await axios.post(`http://localhost:8080/social-login`, {
        user
    });

    return res;
};

export const forgotPasswordService = async email => {
    const res = await axios.put(`http://localhost:8080/forgot-password`, {
        email
    });

    return res;
};

export const resetPasswordService = async ({ resetPasswordLink, newPassword }) => {
    const res = await axios.put(`http://localhost:8080/reset-password`, {
        resetPasswordLink,
        newPassword
    });

    return res;
};