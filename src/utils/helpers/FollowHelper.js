import { isAuthenticated } from "../services/auth/authService";

export const checkFollow = user => {
    const jwt = isAuthenticated();
    // one id has many other ids (followers) and vice versa
    const match = user.followers.find(follower => follower._id === jwt.user._id);
    return match;
}

export const clickFollowButton = async (followservice, setFollowing, following, userID) => {
    const { user } = isAuthenticated();
    try {
        console.log("HOLA")
        await followservice(user._id, userID);
        setFollowing(!following);
    }
    catch (err) {
        console.log(err)
    }
}