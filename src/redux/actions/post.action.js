import { types } from "../types/types";
import { createPostService, deletePostIdService, getPostByService, likePostService, unLikePostService } from "../../utils/services/post/postService";

export const getPostByUserAction = (userID, setLoading, page) => async (dispatch, getState) => {
    try {
        setLoading(true);
        const { data } = await getPostByService(userID, page);

        dispatch({
            type: types.postsByUser,
            payload: data
        });
        setLoading(false)
    } catch (error) {
        console.log(error)
    }
}

export const createPostAction = (inputValues) => async (dispatch, getState) => {
    try {
        const { data } = await createPostService(inputValues);
        dispatch({
            type: types.createPost,
            payload: data
        })
    } catch (error) {
        console.log(error)
    }
}

export const deletePostAction = (postId, setLoading, userID, page) => async (dispatch, getState) => {
    try {
        setLoading(true);
        await deletePostIdService(postId);
        const { data } = await getPostByService(userID, page);
        setLoading(false);
        dispatch({
            type: types.deletePost,
            payload: data
        })
    } catch (error) {
        console.log(error)
    }
}

export const likePostAction = (postId, checkedLike) => async (dispatch, getState) => {
    try {
        let callApi = checkedLike ? unLikePostService : likePostService;
        const { data } = await callApi(postId);
        console.log(data)
        dispatch({
            type: types.likeUnlikePost,
            payload: data
        })
    } catch (error) {
        console.log(error)
    }
}