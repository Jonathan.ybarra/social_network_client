import { types } from "../types/types";

export const signinAction = (user) => async (dispatch, getState) => {
    try {
        dispatch({
            type: types.userLogged,
            payload: user
        })
    } catch (error) {
        console.log(error)
    }
}

export const signoutAction = () => async (dispatch, getState) => {
    try {
        dispatch({
            type: types.userLogout,
            payload: {}
        })
    } catch (error) {
        console.log(error)
    }
}