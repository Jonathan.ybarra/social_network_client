import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { postReducer } from './reducers/post.reducer';
import { userReducer } from './reducers/user.reducer';
import thunk from 'redux-thunk';

const reducers = combineReducers({
    posts: postReducer,
    userLogged: userReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
    reducers,
    composeEnhancers(applyMiddleware(thunk)))
