import { types } from "../types/types";

const initialState = {
    posts:{
        posts:[]
    },
}

export const postReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.postsByUser:
            return {
                ...state,
                posts: action.payload
            };
        case types.createPost:
            const posts = [...state.posts];
            posts.push(action.payload)
            return {
                ...state,
                posts
            };
        case types.deletePost: {
            // const { payload } = action;
            // const posts = state.posts.posts.filter(post => payload !== post._id);
            // console.log({
            //     ...state,
            //     posts
            // })
    
            // console.log('posssssssssststtttttt')
            // console.log(state)
            return {
                ...state,
                posts: action.payload
            };
        }
        case types.likeUnlikePost: {
            const { payload } = action;
            const posts = state.posts.filter(post => payload._id !== post._id);
            posts.push(payload);
            return {
                ...state,
                posts
            };
        }
        default:
            return state;
    }
}