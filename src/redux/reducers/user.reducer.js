import { types } from "../types/types";

const initialState = {}

export const userReducer = (state = initialState, action) => {
    const { payload } = action;
    switch (action.type) {
        case types.userLogged:
            return {
                ...state,
                ...payload
            };
        case types.userLogout:
            return {
                ...payload
            };
        default:
            return state;
    }
}