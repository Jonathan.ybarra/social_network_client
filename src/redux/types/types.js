export const types = {
    postsByUser: "[POSTS] get Post by users",
    createPost: "[CREATRE_POST] create post by author user",
    deletePost: "[DELETE_POST] delete post by user",
    likeUnlikePost: "[LIKE_UNLIKE_POST] like or unlike post with one user",
    userLogged: "[USER_LOGGED] get user logged",
    userLogout: "[USER_LOGOUT] remove user state"
}