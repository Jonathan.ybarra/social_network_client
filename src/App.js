import React from 'react';
import { Switch } from 'react-router-dom';
import routes from './routes/routes';
import { RouteWithSubRoutes } from './routes/RouteWithSubRoutes';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import './App.css';

export const App = () => {
  return (
    <Provider store={store}>
      <div>
        <Switch>
          {
            routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)
          }
        </Switch>
      </div>
    </Provider>
  )
}
