import React, { useEffect } from 'react';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from '../../hooks/useForm';
import { defaultProfilePhoto } from '../../utils/helpers/default_profile_photo';
import { searchUserService } from '../../utils/services/user/UserService';
import { MinimalSpinner } from '../UI/MinimalSpinner/MinimalSpinner';
import './search.css';
export const SearchUser = () => {

    const history = useHistory();
    const [users, setusers] = useState([]);
    const [loading, setloading] = useState(false);
    const [values, handleInputChange, resetForm] = useForm({
        searchUser: ''
    })

    useEffect(() => {
        values.searchUser.length > 0 && updateSearchName()
    }, [values.searchUser])

    const updateSearchName = async () => {
        setloading(true);
        try {

            const { data } = await searchUserService(values.searchUser);
            setusers(data);
            console.log(data)
        }
        catch (e) {
            console.log(e)
        }
        finally {
            setloading(false);
        }
    }

    const redirectUser = (user) => {
        history.push(`/ui/user/${user._id}`);
        resetForm();
    }

    return (
        <>
            <div className="container-search">
                <div className="bmd-form-group bmd-collapse-inline pull-xs-right ml-3">
                    <button className="btn bmd-btn-icon"
                        htmlFor="search"
                        data-toggle="collapse"
                        data-target="#collapse-search"
                        aria-expanded="false"
                        aria-controls="collapse-search"
                    >
                        <i className="material-icons">search</i>
                    </button>
                    <span id="collapse-search" className="collapse">
                        <input
                            autoComplete="off"
                            className="form-control"
                            name="searchUser"
                            type="text"
                            id="search"
                            placeholder="search users..."
                            value={values.searchUser}
                            onChange={handleInputChange}
                        />
                    </span>

                    {
                        values.searchUser.length > 0 &&
                        <div className="container-results">
                            {
                                loading ?
                                    <center>
                                        <MinimalSpinner
                                            width={'25px'}
                                            height={'25px'}
                                            border={'10px solid #fff'}
                                        />
                                    </center>
                                    :
                                    (
                                        <div className="search-results">
                                            {
                                                users.length > 0 ?
                                                    users.map(user =>
                                                        <div
                                                            className="container-profile d-flex"
                                                            onClick={() => redirectUser(user)}
                                                        >
                                                            <img
                                                                src={user.photo_profile || defaultProfilePhoto}
                                                                alt={user.name}
                                                                style={{ height: '40px', width: '40px', borderRadius: '3em', marginRight: '15px' }}
                                                            />
                                                            <div className="mt-2">
                                                                {user.name}<span>{user.email}</span>
                                                            </div>
                                                        </div>
                                                    )
                                                    :
                                                    <center className="container-empty">
                                                        User not found
                                                    </center>
                                            }
                                        </div>
                                    )
                            }
                        </div>
                    }

                </div>
            </div>
        </>
    )
}
