import React, { useState } from 'react'
import { useEffect } from 'react';

export const Pagination = ({ totalItems, pageIndex, setPageIndex }) => {

    const [totalPages, setTotalPages] = useState(0);

    useEffect(() => {
        setTotalPages(Math.ceil(totalItems / 5))
    }, []);

    return (
        <>
            <nav className="mt-5">
                <ul className="pagination justify-content-center">

                    {
                        pageIndex > 1 &&
                        <li
                            className="page-item"
                            onClick={() => setPageIndex(pageIndex - 1)}
                        >
                            <span className="page-link">Previous</span>
                        </li>
                    }

                    {
                        [...Array(totalPages).keys()]
                            .map((n) => n + 1)
                            .map(page =>
                                <li
                                    className={page === pageIndex ? "page-item active" : "page-item"}
                                    onClick={() => setPageIndex(page)}
                                    key={page}
                                >
                                    <span className="page-link">{page}</span>
                                </li>
                            )
                    }

                    {
                        pageIndex < totalPages &&
                        <li
                            className="page-item"
                            onClick={() => setPageIndex(pageIndex + 1)}
                        >
                            <span className="page-link">Next</span>
                        </li>
                    }
                </ul>
            </nav>
        </>
    )
}
