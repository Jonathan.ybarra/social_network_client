import React, { useState } from 'react';
import { Link, NavLink, useHistory } from 'react-router-dom';
import { defaultProfilePhoto } from '../../../utils/helpers/default_profile_photo';
import { isAuthenticated, signoutService } from '../../../utils/services/auth/authService';
import { logoApp } from '../../../utils/helpers/logoAppPhoto';
import { SearchUser } from '../../SearchUser/SearchUser';
// hooks react redux
import { useDispatch, useSelector } from 'react-redux';
import './navbar.css';
import { useEffect } from 'react';
import { signinAction, signoutAction } from '../../../redux/actions/user.action';

export const Navbar = () => {

    const history = useHistory();
    const userLogged = useSelector(store => store.userLogged);
    const dispatch = useDispatch();
    const [loading, setLoading] = useState(false);
    const { user } = isAuthenticated();

    useEffect(() => {
        dispatch(signinAction(user))
    }, [])

    const signout = async () => {
        try {
            setLoading(true)
            const { data } = await signoutService();
            dispatch(signoutAction())
            setLoading(false);
            history.push('/signin');
        }
        catch (e) {
            setLoading(false);
            console.log(e);
        }
    }

    return (
        <>

            {
                !loading &&
                <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 shadow fixed-top">

                    <Link to={'/ui'}>
                        <img src={logoApp} alt="logoapp" className="logoApp" />
                    </Link>

                    <SearchUser />
                    <ul className="navbar-nav ml-auto">
                        <div className="topbar-divider d-none d-sm-block"></div>
                        <li className="nav-item dropdown no-arrow">
                            <a className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {
                                    isAuthenticated() &&
                                    <>
                                        <span className="mr-2 d-none d-lg-inline text-gray-600 small">
                                            {user.name}
                                        </span>
                                        <img
                                            className="img-profile rounded-circle"
                                            src={userLogged.photo_profile || defaultProfilePhoto}
                                        />
                                    </>
                                }
                            </a>
                            <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                {
                                    isAuthenticated() &&
                                    <Link
                                        to={`/ui/user/${user._id}`}
                                        className="dropdown-item"
                                    >
                                        <i className="fa fa-user fa-sm fa-fw mr-2 text-gray-400"></i> Profile
                                    </Link>
                                }
                                {
                                    isAuthenticated() &&
                                    <Link
                                        to={`/ui/edit-user/${user._id}`}
                                        className="dropdown-item"
                                    >
                                        <i className="fa fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i> Settings
                                    </Link>
                                }

                                <div className="dropdown-divider"></div>
                                {
                                    isAuthenticated() &&
                                    <a className="dropdown-item" href="#" onClick={() => signout()}>
                                        <i className="fa fa-sign-out fa-sm fa-fw mr-2 text-gray-400"></i> Logout
                               </a>
                                }
                            </div>
                        </li>
                    </ul>
                </nav>
            }


            {/* <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                   <div class="input-group">
                       <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." />
                       <div class="input-group-append">
                           <button class="btn btn-primary" type="button">
                               <i class="fa fa-search fa-sm"></i>
                           </button>
                       </div>
                   </div>
               </form> */}

            {/* {
                loading ?
                    <Loading /> :
                    <ul className="nav bg-dark justify-content-end p-2">
                        <li className="nav-item">
                            <NavLink
                                activeClassName="text-warning"
                                className="nav-link text-white"
                                to="/ui/users"
                            >
                                Users
                            </NavLink>
                        </li>
                        {
                            !isAuthenticated() &&
                            (
                                <>
                                    <li className="nav-item">
                                        <NavLink className="nav-link text-white" to="/signin">Signin</NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink className="nav-link text-white" to="/signup">Signup</NavLink>
                                    </li>
                                </>
                            )
                        }

                        {
                            isAuthenticated() &&
                            <>
                                <li className="nav-item">
                                    <NavLink
                                        activeClassName="text-warning"
                                        className="nav-link text-white"
                                        to={`/ui/user/${isAuthenticated().user._id}`}
                                    >
                                        {isAuthenticated().user.name}
                                    </NavLink>
                                </li>

                                <li className="nav-item" onClick={() => signout()}>
                                    <div className="nav-link text-white">Log out</div>
                                </li>
                            </>
                        }

                    </ul>
            } */}
        </>
    )
}
