import React from 'react';
import './minimalSpinner.css';

export const MinimalSpinner = ({ width = '60px', height = '60px'}) => {

    const styleLoad = {
        width,
        height
    }
    return (
        <div className="loading" style={styleLoad}></div>
    )
}
