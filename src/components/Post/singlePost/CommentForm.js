import React, { useState } from 'react';
import { useForm } from '../../../hooks/useForm';
import { commentPostService } from '../../../utils/services/post/postService';
import { ToastContainer, toast } from 'react-toastify';
import Picker from 'emoji-picker-react';

export const CommentForm = ({ postId, getPostId }) => {

    const [showChosenEmoji, setShowChosenEmoji] = useState(false);
    const [inputValues, handleInputChange, resetForm, setStateValues, onEmojiClick] = useForm({
        comment: ''
    });

    const isValidForm = () => {
        const { comment } = inputValues;
        if (!comment.length > 0 || comment.length > 150) {
            return false;
        }
        return true
    }

    const onSubmitNewcomment = async e => {
        e.preventDefault();

        try {
            if (isValidForm()) {
                const comment = { text: inputValues.comment };
                const { data } = await commentPostService(postId, comment);
                console.log(data);
                getPostId(postId);
                resetForm();
            }
            else {
                toast.error("Comment should not be empty less than 150 characters long", {
                    draggablePercent: 60,
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            }
        }
        catch (err) {
            console.log("Error")
        }
    };

    return (
        <>
            <div className="row">
                <div className="col-md-10 offset-md-1">
                    <div className="card my-5 bg-light ">
                        <div className="card-body">
                            <form onSubmit={onSubmitNewcomment}>
                                <input
                                    className="input-post"
                                    type="text"
                                    placeholder="Write your comment.."
                                    autoComplete="off"
                                    name="comment"
                                    value={inputValues.comment}
                                    onChange={handleInputChange}
                                />
                                <button type="submit" className="btn btn-small btn-info btn-raised mt-3 float-right">Comment</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer draggablePercent={60} />
        </>
    )
}
