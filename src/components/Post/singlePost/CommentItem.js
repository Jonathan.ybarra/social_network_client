import React from 'react';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { isAuthenticated } from '../../../utils/services/auth/authService';
import { unCommentPostService } from '../../../utils/services/post/postService';
import { defaultProfilePhoto } from '../../../utils/helpers/default_profile_photo';

export const CommentItem = ({ comment, postId, getPostId }) => {

    const deleteComment = async () => {

        try {
            const { data } = await unCommentPostService(postId, comment);
            console.log(data);
            getPostId(postId);

        }
        catch (err) {
            console.log("Error")
        }
    };

    return (
        <>
            <div className="comments">
                <ul>
                    <li>
                        <div className="user-comment">
                            <img
                                src={comment.postedBy.photo_profile || defaultProfilePhoto}
                                alt={comment._id}
                            />
                            <header>
                                <Link
                                    to={`/ui/user/${comment.postedBy._id}`}
                                    className="name">
                                    {comment.postedBy.name}
                                </Link>
                                <span>{moment(comment.created).fromNow()}</span>
                            </header>


                            <div className="content">
                                <p>{comment.text}</p>
                            </div>

                            {
                                isAuthenticated().user &&
                                isAuthenticated().user._id === comment.postedBy._id &&
                                <span className="reply" onClick={() => deleteComment()}>Delete</span>
                            }

                        </div>
                    </li>
                </ul>
            </div>
        </>
    )
}
