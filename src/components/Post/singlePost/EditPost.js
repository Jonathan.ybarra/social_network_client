import React from 'react'
import { NewPostForm } from '../NewPostForm';

export const EditPost = () => {
    return (
        <div className="pt-95">
            <NewPostForm />
        </div>
    )
}
