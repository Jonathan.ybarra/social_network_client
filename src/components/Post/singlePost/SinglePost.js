import React, { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { getPostIdService, likePostService, unLikePostService } from '../../../utils/services/post/postService';
import { CommentItem } from './CommentItem';
import moment from 'moment';
import './singlePost.css';
import { isAuthenticated } from '../../../utils/services/auth/authService';
import { defaultProfilePhoto } from '../../../utils/helpers/default_profile_photo';
import { CommentForm } from './CommentForm';

export const SinglePost = () => {


    const [like, setlike] = useState(false);
    const [likes, setLikes] = useState(0);
    const [loading, setloading] = useState(false);
    const [postState, setPostState] = useState({
        photo: '',
        postedBy: ''
    });

    const { postId } = useParams();

    useEffect(() => {
        getPostId(postId);
    }, []);

    const getPostId = async (postId) => {
        setloading(true);
        const { data } = await getPostIdService(postId);
        setlike(checkLike(data.likes));
        setLikes(data.likes.length)
        console.log(data);
        setPostState(data);
        setloading(false);
    }

    const checkLike = likes => {
        const userId = isAuthenticated() && isAuthenticated().user._id;
        let match = likes.indexOf(userId) !== -1;
        return match;
    };

    const likeToggle = async () => {
        let callApi = like ? unLikePostService : likePostService;
        const { data } = await callApi(postState._id);
        setlike(!like)
        setLikes(data.likes.length);
    };

    return (
        <>
            {
                !loading &&
                <>
                    <div class="container pt-5">
                        <div id="post">
                            <header>
                                <img
                                    alt={postState._id}
                                    src={postState.postedBy.photo_profile ? postState.postedBy.photo_profile : defaultProfilePhoto}
                                />
                                <p>
                                    <Link
                                        className="name"
                                        to={`/ui/user/${postState.postedBy._id}`}
                                    >
                                        {postState.postedBy.name}
                                    </Link> uploaded a new post.
                                    <span>{moment(postState.created).fromNow()}</span>
                                </p>
                                <div className="option"></div>
                            </header>

                            <div className="content">
                                <p>{postState.body}</p>
                                {
                                    postState.photo &&
                                    postState.photo.length > 0 &&
                                    <img src={postState.photo} alt={postState._id} />
                                }
                                <div id="social">
                                    <div className="post-comments">{postState.comments && postState.comments.length}</div>

                                    <div
                                        className={like ? "likes text-info" : "likes text-secondary"}
                                        onClick={() => likeToggle()}
                                    >
                                        <span
                                            className={
                                                like ?
                                                    "material-icons mr-1 cursor-pointer text-info mt-5" :
                                                    "material-icons mr-1 cursor-pointer text-secondary pt-5"
                                            }
                                        >
                                            thumb_up
                                        </span>
                                        <p>
                                            {likes}
                                        </p>
                                    </div>
                                </div>

                                <CommentForm postId={postState._id} getPostId={getPostId} />
                            </div>

                            {
                                postState.comments &&
                                postState.comments.map(comment =>
                                    <CommentItem
                                        key={comment._id}
                                        comment={comment}
                                        postId={postState._id}
                                        getPostId={getPostId}
                                    />
                                )
                            }
                        </div>
                    </div>

                </>

            }
        </>
    )
}


{/* <div className="card">
                    <div className="card-body">
                        {
                            postState.photo === '' ?
                                <div>HOLA</div>
                                :
                                <SinglePostBodyWithImg postState={postState} />
                        }

                        <div className="d-flex mt-5 justify-content-around">

                          <div className="d-flex pt-2">
                                <Link
                                    to={`/ui/post/${postState._id}`}
                                    className="text-muted ml-1 badge"
                                >
                                    {moment(postState.created).fromNow()}
                                </Link>
                                <span className="material-icons right">
                                    more_vert
                                </span>
                            </div> 
                        </div>
                    </div>
                </div>
                */}