import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { MinimalSpinner } from '../UI/MinimalSpinner/MinimalSpinner';
import { PostItem } from './postItem/PostItem';
import { PostItemModalDelete } from './postItem/PostItemModalDelete';
// hooks react redux
import { useDispatch, useSelector } from 'react-redux';
import { deletePostAction, getPostByUserAction } from '../../redux/actions/post.action';
import { Pagination } from '../UI/pagination/Pagination';
 
export const PostScreen = () => {

    const { id: userID } = useParams();
    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(false);
    const [postDeleteState, setPostDeleteState] = useState(null);
    const postsByUsersId = useSelector(store => store.posts.posts);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getPostByUserAction(userID, setLoading, page))
    }, [userID, page])

    const deletePost = async () => {
        dispatch(deletePostAction(postDeleteState._id, setLoading, userID, page));
    }

    return (
        <>
            {
                loading && <center><MinimalSpinner /></center>
            }

            {
                !loading &&
                <div className="row">
                    <div className="col-md-10 offset-md-1 my-5">
                        {
                            postsByUsersId.posts &&
                            postsByUsersId.posts.map(post =>
                                <PostItem
                                    post={post}
                                    key={post._id}
                                    setPostDeleteState={setPostDeleteState}
                                />
                            )
                        }
                        <Pagination
                            totalItems={postsByUsersId.totalItems}
                            pageIndex={page}
                            setPageIndex={setPage}
                        />
                    </div>
                    <PostItemModalDelete
                        deletePost={deletePost}
                    />
                </div>
            }
        </>
    )
}
