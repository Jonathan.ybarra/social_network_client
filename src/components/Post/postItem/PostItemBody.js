import React from 'react';
import { Link } from 'react-router-dom';

export const PostItemBody = ({ post }) => {
    return (
        <>
            <div className="card-body">
                {
                    post.photo &&
                    <Link to={`/ui/post/${post._id}`}>
                    <center className="mb-3">
                        <img
                            alt={post._id}
                            className="post__photo"
                            src={post.photo}
                            data-toggle="modal"
                            data-target="#IMGModal"
                        />
                    </center>
                    </Link>
                }
                <p style={{ color: '727988' }}>
                    {post.body}
                </p>
            </div>

        
        </>

    )
}
