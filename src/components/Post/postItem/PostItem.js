import React from 'react';
import { PostItemBody } from './PostItemBody';
import { PostItemFooter } from './PostItemFooter';
import { PostItemHeader } from './PostItemHeader';

export const PostItem = ({ post, setPostDeleteState }) => {
    return (
        <div className="card">
            <PostItemHeader post={post} setPostDeleteState={setPostDeleteState} />

            <PostItemBody post={post} />

            <PostItemFooter post={post} />

        </div>
    )
}
