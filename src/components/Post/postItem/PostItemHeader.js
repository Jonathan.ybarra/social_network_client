import React from 'react';
import { Link } from 'react-router-dom';
import { defaultProfilePhoto } from '../../../utils/helpers/default_profile_photo';
import { isAuthenticated } from '../../../utils/services/auth/authService';
import moment from 'moment';

export const PostItemHeader = ({ post, setPostDeleteState }) => {

    return (
        <div className="card-header">
            <div className="d-flex justify-content-between">
                <div className="d-flex">
                    <img
                        alt={post.postedBy._id}
                        className="postedby__photo"
                        src={post.postedBy.photo_profile ? post.postedBy.photo_profile : defaultProfilePhoto}
                    />
                    <Link to={`/ui/user/${post.postedBy._id}`} className="pt-2 text-muted ml-3">{post.postedBy.name}</Link>
                </div>

                <div className="d-flex">
                    <Link
                        to={`/ui/post/${post._id}`}
                        className="text-muted ml-1 badge"
                    >
                        {moment(post.created).fromNow()}
                    </Link>


                    {
                        isAuthenticated().user &&
                        isAuthenticated().user._id === post.postedBy._id &&
                        <div className="dropdown right">
                            <span
                                className="material-icons dropdown-toggle cursor-pointer"
                                id="dropdownMenuButton"
                                data-toggle="dropdown"
                            >
                                more_vert
                            </span>
                            <div className="dropdown-menu" style={{position:"absolute"}}>
                                <Link
                                    className="dropdown-item text-info"
                                    to={`/ui/edit-post/${post._id}`}
                                >
                                    Edit
                                </Link>
                                <button
                                    className="dropdown-item text-danger"
                                    data-toggle="modal"
                                    data-target="#deletePostModal"
                                    onClick={() => setPostDeleteState(post)}
                                >
                                    Delete
                            </button>
                            </div>
                        </div>
                    }
                </div>
            </div>
        </div>
    )
}
