import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { isAuthenticated } from '../../../utils/services/auth/authService';
import { likePostService, unLikePostService } from '../../../utils/services/post/postService';

export const PostItemFooter = ({ post }) => {

    const [like, setlike] = useState(false);
    const [likes, setLikes] = useState(0);

    useEffect(() => {
        setlike(checkLike(post.likes));
        setLikes(post.likes.length) 
    }, []);

    const checkLike = likes => {
        const userId = isAuthenticated() && isAuthenticated().user._id;
        let match = likes.indexOf(userId) !== -1;
        return match;
    };

    const likeToggle = async () => {
        let callApi = like ? unLikePostService : likePostService;
        const { data } = await callApi(post._id);
        setlike(!like)
        setLikes(data.likes.length);
    };

    return (
        <div className="card-footer ">
            <div className="d-flex">
                <p className={like ? "pt-1 mr-1 text-info" : "pt-1 mr-1 text-secondary"}>
                    {likes}
                </p>
                <span
                    className={
                        like ?
                            "material-icons mr-3 cursor-pointer text-info" :
                            "material-icons mr-3 cursor-pointer text-secondary"
                    }
                    onClick={() => likeToggle()}
                >
                    thumb_up
                </span>
                <div className="d-flex pt-1">
                    <p className="mr-1 text-info">{post.comments.length}</p>
                    <Link
                        to={`/ui/post/${post._id}`}
                        className="material-icons text-info"
                    >
                        comment
                    </Link>
                </div>
            </div>
        </div >
    )
}
