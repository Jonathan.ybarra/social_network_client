import React, { useEffect, useState } from 'react';
import { useForm } from '../../hooks/useForm';
import { getPostIdService, updatePostService } from '../../utils/services/post/postService';
// hooks react redux
import { useDispatch } from 'react-redux';
import { createPostAction } from '../../redux/actions/post.action';
import { createRef } from 'react';
import { convertBase64 } from '../../utils/helpers/ConvertImageToBase64';
import { ToastContainer, toast } from 'react-toastify';
import { useHistory, useParams } from 'react-router-dom';
import { isAuthenticated } from '../../utils/services/auth/authService';
import { useFileUpload } from 'use-file-upload'
import Picker from 'emoji-picker-react';
import 'react-toastify/dist/ReactToastify.css';
import './post.css';
import 'animate.css/animate.css';

export const NewPostForm = ({ getUserId, userID }) => {

    const dispatch = useDispatch();
    const history = useHistory()
    const imgResult = createRef();
    const clearImg = createRef();
    const [chosenEmoji, setChosenEmoji] = useState(null);
    const [showChosenEmoji, setShowChosenEmoji] = useState(false);
    const { postId } = useParams();
    const [file, selectFile] = useFileUpload()
    const [inputValues, handleInputChange, resetForm, setStateValues, onEmojiClick, handleInputChangeFile] = useForm({
        body: '',
        photo: ''
    });

    useEffect(() => {
        postId && getPost();
    }, []);

    const getPost = async () => {
        const { data } = await getPostIdService(postId);
        setStateValues({
            body: data.body,
            photo: data.photo
        })
    }

    const isValidForm = () => {
        const { body } = inputValues;
        if (body.length === 0) {
            return false;
        }
        return true;
    };

    const onSubmitNewPost = async e => {
        e.preventDefault();
        try {
            if (isValidForm()) {
                const filteTo64 = file ? await convertBase64(file.file) : null;

                dispatch(createPostAction({ ...inputValues, photo: filteTo64 }))
                getUserId(userID);
                resetForm();
            }
            else {
                toast.error("Must write something..", {
                    draggablePercent: 60,
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            }
        }
        catch (err) {
            console.log(err)
        }
    };

    const onSubmitEditPost = async e => {
        e.preventDefault();
        const { user } = isAuthenticated();
        try {
            if (isValidForm()) {
                const filteTo64 = file ? await convertBase64(file.file) : null;
                await updatePostService(postId, { ...inputValues, photo: filteTo64 });
                history.push(`/ui/user/${user._id}/posts`)
                getUserId(userID);
            }
            else {
                toast.error("Must write something..", {
                    draggablePercent: 60,
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            }
        }
        catch (err) {
            console.log(err)
        }
    };

    return (
        <>
            <div className="row">
                <div className="col-md-10 offset-md-1">
                    <div className="card my-5 bg-light">
                        <div className="card-body">
                            <form onSubmit={postId ? onSubmitEditPost : onSubmitNewPost}>
                                <textarea
                                    className="input-post"
                                    type="text"
                                    placeholder="Write your new post.."
                                    autoComplete="off"
                                    name="body"
                                    value={inputValues.body}
                                    onChange={handleInputChange}
                                />
                                <div className="d-flex">
                                    <div className="mt-3 mr-2">

                                        <span
                                            className="material-icons"
                                            style={{ fontSize: "45px", cursor: 'pointer' }}
                                            onClick={() => setShowChosenEmoji(!showChosenEmoji)}
                                        >
                                            mood
                                        </span>
                                        {
                                            showChosenEmoji &&
                                            <div
                                                style={{ position: 'absolute', zIndex: '5' }}
                                                className=""
                                            >
                                                <Picker
                                                    onEmojiClick={onEmojiClick}
                                                    name="body"
                                                />
                                            </div>
                                        }
                                    </div>
                                    <div>
                                        <span
                                            className="btn btn-small btn-file btn-raised  material-icons mt-3"
                                            onClick={() => selectFile()}
                                        >
                                            add_photo_alternate
                                    </span>


                                        {file && <img src={file.source} alt='preview' className="img__result" />}
                                    </div>
                                </div>

                                <button
                                    type="submit"
                                    className="btn btn-small btn-info btn-raised mt-3 float-right"
                                >
                                    {postId ? 'Edit Publish' : 'Publish'}
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer draggablePercent={60} />
        </>
    )
}
