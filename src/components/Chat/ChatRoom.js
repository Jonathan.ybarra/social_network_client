import React from 'react';
import moment from 'moment';
import { isAuthenticated } from '../../utils/services/auth/authService';
import './chat.css';
import { defaultProfilePhoto } from '../../utils/helpers/default_profile_photo';

export const ChatRoom = ({ messages, handleInputChange, inputValues, submitChatMessage }) => {
    const { user } = isAuthenticated();

    return (
        <>
            {/* <div class="day">Hoy</div> */}
            <ol class="chat">
                {
                    messages.map(msg =>
                        <li
                            key={msg._id}
                            className={msg.sender._id === user._id ? "self" : 'other'}
                        >
                            <div className="avatar" data-toggle="tooltip" data-placement="bottom" title={msg.sender.name}>
                                <img src={msg.sender.photo_profile || defaultProfilePhoto} draggable="false" />
                            </div>
                            <div className="msg">
                                <p>{msg.message.substr(0, 148)}</p>
                                <p>{msg.message.substr(148, msg.message.lenght)}</p>

                                <time>{moment(msg.createdAt).format("H")}:{moment(msg.createdAt).format("m")}</time>
                            </div>
                        </li>
                    )
                }
            </ol>
            <form onSubmit={submitChatMessage}>

                <input
                    className="textarea"
                    name="chatMessage"
                    autoComplete='off'
                    type="text"
                    placeholder="Type here!"
                    onChange={handleInputChange}
                    value={inputValues.chatMessage}
                />
                <div className="emojis"></div>
            </form>
        </>
    )
}
