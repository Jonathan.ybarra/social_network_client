import React, { useEffect } from 'react';
import { useForm } from '../../hooks/useForm';
import { isAuthenticated } from '../../utils/services/auth/authService';
import io from 'socket.io-client';
import { useState } from 'react';
import { getAllChatService } from '../../utils/services/chat/ChatService';
import { ChatRoom } from './ChatRoom';

export const ChatScreen = () => {

    const { user: userLogged } = isAuthenticated();
    const [messages, setMessages] = useState([])
    const [inputValues, handleInputChange, resetForm] = useForm({
        chatMessage: ''
    });
    let socket;


    useEffect(() => {
        let server = 'http://localhost:8080';
        socket = io(server)
        socket.on('Output Chat Message', messageFromBackend => {
            // setMessages(messages.concat(messageFromBackend))
            // console.log(messageFromBackend)
            totalMessages()
        })
    }, [inputValues])


    useEffect(() => {
        totalMessages();
    }, [])

    const submitChatMessage = (e) => {
        e.preventDefault();
        socket.emit('Input Chat Message', {
            user: userLogged,
            message: inputValues.chatMessage,
            type: 'Image'
        })
        resetForm();
    }

    const totalMessages = async () => {
        try {
            const { data } = await getAllChatService();
            setMessages(data)
            console.log(data)
        }
        catch (err) {
            console.log(err)
        }
    }

    return (
        <>
            {/* <div>
                <p style={{ fontSize: '2rem', textAlign: 'center', marginTop: '90px' }}>Real Time Chat</p>
            </div> */}

            <div
                className='bg-white'
                style={{ maxWidth: '1000px', margin: '0 auto', minHeight: '700px' }}
            >

                <ChatRoom
                    messages={messages}
                    handleInputChange={handleInputChange}
                    inputValues={inputValues}
                    submitChatMessage={submitChatMessage}
                />
                {
                    messages.length === 0 &&
                    <center className="mt-5">No user has sent messages..</center>
                }

            </div>

        </>
    )
}
