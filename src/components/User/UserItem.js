import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { defaultProfilePhoto } from '../../utils/helpers/default_profile_photo'
import { checkFollow, clickFollowButton } from '../../utils/helpers/FollowHelper';
import { followUserService, unfollowUserService } from '../../utils/services/user/FollowUnFollow';

export const UserItem = ({ user, getUsers }) => {

    const history = useHistory();
    const [following, setFollowing] = useState(false);

    useEffect(() => {
        setFollowing(checkFollow(user))
    }, [user]);

    const followButton = async () => {
        await clickFollowButton(followUserService, setFollowing, following, user._id);
        getUsers();
    }

    const unFollowButton = async () => {
        await clickFollowButton(unfollowUserService, setFollowing, following, user._id);
        getUsers();
    }

    return (
        <>
            <a className="list-group-item border-bottom">
                <img
                    alt={user._id}
                    className="img-profile-list"
                    src={user.photo_profile ? user.photo_profile : defaultProfilePhoto}
                />

                <div className="bmd-list-group-col">
                    <p
                        onClick={() => history.push(`/ui/user/${user._id}`)}
                        className="list-group-item-heading cursor-pointer"
                    >
                        {user.name}
                    </p>
                    <p className="list-group-item-text">{user.email}</p>
                </div>
                <span
                    style={{ fontSize: '13px', paddingLeft: '10px' }}
                    className=
                    {
                        following ?
                            "label label-default label-pill pull-xs-right text-warning cursor-pointer" :
                            "label label-default label-pill pull-xs-right text-info cursor-pointer"
                    }
                    onClick={
                        () =>
                            following ?
                                unFollowButton() :
                                followButton()
                    }
                >
                    {following ? 'Un follow' : 'Follow'}
                </span>
            </a>
        </>
    )
}
