import React, { useState } from 'react'
import { findPeopleService } from '../../../utils/services/user/UserService';

export const FindPeople = () => {

    const [users, setUsers] = useState([]);

    const searchPeople = async () => {
        const { data } = await findPeopleService();
        setUsers(data)
    }

    return (
        <div>

        </div>
    )
}
