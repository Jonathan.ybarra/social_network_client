import React, { useEffect } from 'react';
import { NavLink, Switch } from 'react-router-dom'
import { RouteWithSubRoutes } from '../../../routes/RouteWithSubRoutes';
// hooks react redux
import { useDispatch, useSelector } from 'react-redux';
import { getPostByUserAction } from '../../../redux/actions/post.action';
import './profile.css';

export const ProfileTabs = ({ followers, following, userID, routes }) => {

    const postsByUsersId = useSelector(store => store.posts.posts);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getPostByUserAction(userID))
    }, [userID]);

    const style = {
        background: 'rgba(78, 75, 75, 0.05)',
        color: 'ffffff'
    }

    return (
        <>
            <ul className="data-user mt-3">
                <li>
                    <NavLink
                        activeClassName='active-profile-tabs'
                        to={`/ui/user/${userID}/posts`}
                        className="profile-tabs"
                    >
                        <strong>{postsByUsersId.totalItems || 0}</strong>
                        <span>Posts</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        activeClassName='active-profile-tabs'
                        to={`/ui/user/${userID}/followers`}
                        className="profile-tabs"
                    >
                        <strong>{followers.length}</strong>
                        <span>Followers</span>
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        activeClassName='active-profile-tabs'
                        to={`/ui/user/${userID}/followings`}
                        className="profile-tabs"
                    >
                        <strong>{following.length}</strong>
                        <span>Following</span>
                    </NavLink>
                </li>
            </ul>

            <Switch>
                {
                    routes.map((route, i) => <RouteWithSubRoutes key={i} {...route} />)
                }
            </Switch>
        </>
    )
}
