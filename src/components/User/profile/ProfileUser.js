import React, { useEffect, useState } from 'react';
import { Link, Redirect, useHistory, useParams } from 'react-router-dom';
import { defaultProfilePhoto } from '../../../utils/helpers/default_profile_photo';
import { isAuthenticated } from '../../../utils/services/auth/authService';
import { getPostByService } from '../../../utils/services/post/postService';
import { userByIDService, followUserService } from '../../../utils/services/user/UserService';
import { NewPostForm } from '../../Post/NewPostForm';
import { Loading } from '../../UI/Loading/Loading';
import { FollowProfileButton } from '../followProfile/FollowProfileButton';
import { ProfileHeader } from './ProfileHeader';
import { ProfileTabs } from './ProfileTabs';
// hooks react redux
// import {useDispatch, useSelector} from 'react-redux';
// import { getPostByUserAction } from '../../../redux/actions/post.action';
import './profile.css';

export const ProfileUser = ({ routes }) => {

    const { id: userID } = useParams();
    const history = useHistory();
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [following, setFollowing] = useState(false);
    const [userState, setUserState] = useState({
        following: [],
        followers: []
    });
    const [redirectToSignin, setRedirectToSignin] = useState(false);

    // const dispatch = useDispatch();
    // const postsByUsersId = useSelector(store => store.posts.posts);

    useEffect(() => {
        userId(userID);
        // dispatch(getPostByUserAction(userID))
        history.push(`/ui/user/${userID}/posts`);
    }, [userID]);

    const userId = async (id) => {
        setLoading(true);
        try {
            const { data } = await userByIDService(id);
            setUserState(data);
            setFollowing(checkFollow(data))
            setLoading(false);
        }
        catch ({ response }) {
            if (response) {
                console.log(response.data.error);
            }
            else {
                console.log('Server down')
            }
            setLoading(false);
            setRedirectToSignin(true);
        }
    }


    const checkFollow = user => {
        const jwt = isAuthenticated();
        // one id has many other ids (followers) and vice versa
        const match = user.followers.find(follower => follower._id === jwt.user._id);
        return match;
    }

    const clickFollowButton = async (followservice) => {
        const { user } = isAuthenticated();
        try {
            setLoading(true);
            await followservice(user._id, userID);
            userId(userID);
            setFollowing(!following);
            setLoading(false);
        }
        catch (err) {
            setLoading(false);
            setError(err);
        }
    }

    return (
        <>
            {
                loading &&
                <Loading />
            }

            {
                !loading &&
                <div className="content-profile-page pt-95">
                    <div className="profile-user-page card">
                        <ProfileHeader user={userState} />

                        {
                            isAuthenticated().user &&
                                isAuthenticated().user._id === userState._id ?
                                <Link
                                    to={`/ui/edit-user/${userState._id}`}
                                    className="btn-edit-profile"
                                >
                                    Edit Profile
                                </Link>
                                :
                                <FollowProfileButton
                                    following={following}
                                    onclickFollowButton={clickFollowButton}
                                />
                        }

                        <div className="user-profile-data">
                            <h1>{userState.name}</h1>
                            <p>{userState.link_profile_job}</p>
                        </div>
                        <div className="description-profile">
                            {
                                userState.preference_one &&
                                `${userState.preference_one} |`
                            }
                            {
                                userState.preference_two &&
                                ` ${userState.preference_two} |`
                            }
                            <a href="#" title="bullgit">
                                <strong> {userState.email}</strong>
                            </a>
                            {
                                userState.about &&
                                `| ${userState.about}`
                            }
                        </div>

                        {
                            isAuthenticated().user &&
                            isAuthenticated().user._id === userState._id &&
                            <NewPostForm userID={userID} getUserId={userId} />
                        }


                        <ProfileTabs
                            followers={userState.followers}
                            following={userState.following}
                            userID={userID}
                            routes={routes}
                        />
                    </div>
                </div>
            }

            {
                redirectToSignin && <Redirect to='/signin' />
            }
        </>
    )
}


// <div className="row py-5 px-4">
{/* <div className="col-md-9 mx-auto">
<div className="bg-white overflow-hidden">
    <div className="px-4 pt-0 pb-4 cover">
        <div className="media align-items-end profile-head">
            <div className="profile mr-3">
                <img
                    alt={userState._id}
                    width="130"
                    className="my-2"
                    src={userState.photo_profile ? userState.photo_profile : defaultProfilePhoto}
                />
                {
                    isAuthenticated().user &&
                        isAuthenticated().user._id === userState._id ?
                        <Link
                            to={`/ui/edit-user/${userState._id}`}
                            className="btn btn-raised btn-dark btn-sm btn-block"
                        >
                            Edit Profile
                        </Link>
                        :
                        <FollowProfileButton
                            following={following}
                            onclickFollowButton={clickFollowButton}
                        />
                }
            </div>
            <div className="media-body mb-5">
                <h4 className="mt-0 mb-0">{userState.name}</h4>
                <p className="small mb-4">
                    {userState.email}
                </p>
            </div>
        </div>
    </div>
    <div className="bg-light p-4 d-flex justify-content-end text-center">
        <ul className="list-inline mb-0">
            <li className="list-inline-item">
                <h5 className="font-weight-bold mb-0 d-block">215</h5><small className="text-muted"> <i className="fas fa-image mr-1"></i>Photos</small>
            </li>
            <li className="list-inline-item">
                <h5 className="font-weight-bold mb-0 d-block">745</h5><small className="text-muted"> <i className="fas fa-user mr-1"></i>Followers</small>
            </li>
            <li className="list-inline-item">
                <h5 className="font-weight-bold mb-0 d-block">340</h5><small className="text-muted"> <i className="fas fa-user mr-1"></i>Following</small>
            </li>
        </ul>
    </div>
    <div className="px-4 py-3">
        <h5 className="mb-0">About</h5>
        <div className="p-4 shadow-sm bg-light">
            <p className="font-italic mb-0">Web Developer</p>
        </div>
    </div>
    <div className="py-4 px-4">
        <div className="d-flex align-items-center justify-content-between mb-3">
            <h5 className="mb-0">Recent photos</h5><a href="#" className="btn btn-link text-muted">Show all</a>
        </div>
        <div className="row">
            <div className="col-lg-6 mb-2 pr-lg-1">
                <img src="https://images.unsplash.com/photo-1469594292607-7bd90f8d3ba4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80" alt="" className="img-fluid rounded shadow-sm" />
            </div>
            <div className="col-lg-6 mb-2 pl-lg-1">
                <img src="https://images.unsplash.com/photo-1493571716545-b559a19edd14?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80" alt="" className="img-fluid rounded shadow-sm" />
            </div>
            <div className="col-lg-6 pr-lg-1 mb-2">
                <img src="https://images.unsplash.com/photo-1453791052107-5c843da62d97?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80" alt="" className="img-fluid rounded shadow-sm" />
            </div>
            <div className="col-lg-6 pl-lg-1">
                <img src="https://images.unsplash.com/photo-1475724017904-b712052c192a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80" alt="" className="img-fluid rounded shadow-sm" />
            </div>
        </div>
    </div>
</div>
</div>
</div> */}
