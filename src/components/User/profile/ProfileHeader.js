import React from 'react';
import { defaultProfilePhoto } from '../../../utils/helpers/default_profile_photo';

export const ProfileHeader = ({ user }) => {

    return (
        <div className="img-user-profile">
            <img
                className="profile-bgHome"
                src="https://www.tek4-newmedia.co.uk/images/code.jpg"
                alt="cover"
            />
            <img
                alt={user._id}
                className="avatar"
                src={user.photo_profile ? user.photo_profile : defaultProfilePhoto}
            />
        </div>
    )
}

