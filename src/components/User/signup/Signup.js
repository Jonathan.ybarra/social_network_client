import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useForm } from '../../../hooks/useForm';
import { signupService } from '../../../utils/services/auth/authService';
import { Loading } from '../../UI/Loading/Loading';
import { SocialLogin } from '../SocialLogin';
import { FormSignup } from './FormSignup';
import './signup.css';

export const Signup = () => {

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [userCreated, setUserCreated] = useState(false);
    const [inputValues, handleInputChange, resetForm] = useForm({
        name: '',
        email: '',
        password: '',
    });

    const onSignup = async (e) => {
        e.preventDefault();
        setLoading(true);
        const user = {
            name: inputValues.name,
            email: inputValues.email,
            password: inputValues.password
        }
        try {
            await signupService({ ...user })
            resetForm();
            setError(null);
            setUserCreated(true);
            setLoading(false)
        }
        catch ({ response }) {
            setError(response.data.error);
            setLoading(false);
        }
    }

    return (
        <>
            {
                loading ? <Loading /> :
                    <div className="container">
                        {
                            error &&
                            <div className="alert alert-danger alert-dismissible fade show mt-5 z-index" role="alert">
                                {error}
                                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        }

                        {
                            userCreated &&
                            <div className="alert alert-info mt-5">New account is successfully created. Please <Link to={'/signin'}>Sign In.</Link></div>
                        }

                        <div className="container_form">
                            <div className="card_form">
                                <div className="row">
                                    <div className="col-12 col-md-6">
                                        <div className="left_container">
                                            <center><header className="my-4">Create new account</header></center>
                                            <FormSignup
                                                onSignup={onSignup}
                                                inputValues={inputValues}
                                                handleInputChange={handleInputChange}
                                            />
                                            <SocialLogin buttonTitle={"Signup with google"} />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="rigth_container bg-dark">
                                            <div className="box">
                                                <header>Social Network</header>
                                                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Repudiandae non libero impedit ratione quasi, voluptates deleniti eos velit quos. Illo magni, eos facere quibusdam minus ipsum quaerat sunt quae natus.</p>
                                                <div className="p-3 d-flex flex-row justify-content-center align-items-center member border-top">
                                                    <span>Already signed up? </span> 
                                                    <Link to="/signin" className="text-decoration-none ml-2">Login</Link>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            }
        </>
    )
}
