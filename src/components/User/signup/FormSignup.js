import React from 'react'

export const FormSignup = ({ onSignup, inputValues, handleInputChange }) => {
    return (
        <form onSubmit={onSignup}>
            <div className="form-group">
                <label htmlFor="" className="bmd-label-floating">Name</label>
                <input
                    autoComplete="off"
                    name="name"
                    type="text"
                    className="form-control danger"
                    value={inputValues.name}
                    onChange={handleInputChange}
                />
            </div>

            <div className="form-group">
                <label htmlFor="" className="bmd-label-floating">Email</label>
                <input
                    autoComplete="off"
                    name="email"
                    type="Email"
                    className="form-control"
                    value={inputValues.email}
                    onChange={handleInputChange}
                />
            </div>

            <div className="form-group">
                <label htmlFor="" className="bmd-label-floating">Password</label>
                <input
                    autoComplete="off"
                    name="password"
                    type="password"
                    className="form-control"
                    value={inputValues.password}
                    onChange={handleInputChange}
                />
            </div>

            <button type="submit" className="btn btn-raised btn-block mt-5 btn-secondary signup">Get started</button>
        </form>
    )
}
