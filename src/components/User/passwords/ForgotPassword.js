import React, { useState } from "react";
import { useForm } from "../../../hooks/useForm";
import { forgotPasswordService } from "../../../utils/services/auth/authService";

export const ForgotPassword = () => {

    const [error, setError] = useState('');
    const [message, setMessage] = useState('');
    const [inputValues, handleInputChange, resetForm] = useForm({
        email: ''
    });

    const forgotPassword = async e => {
        e.preventDefault();
        try {
            const { data } = await forgotPasswordService(inputValues.email)
            setMessage(data.message);
            resetForm();
        }
        catch ({ response }) {
            setError(response.data.message)
        }
    };

    return (
        <>
            <div className="container login_container">
                <h2 className="mt-5 mb-5">Ask for Password Reset</h2>

                {message && (
                    <h4 className="alert alert-success">{message}</h4>
                )}
                {error && (
                    <h4 className="alert alert-danger">{error}</h4>
                )}

                <form onSubmit={forgotPassword}>
                    <div className="form-group mt-5">
                        <input
                            autoFocus
                            autoComplete="off"
                            type="email"
                            className="form-control"
                            placeholder="Your email address"
                            value={inputValues.email}
                            name="email"
                            onChange={handleInputChange}
                        />
                    </div>
                    <button
                        className="btn btn-raised btn-primary"
                        type="submit"
                    >
                        Send Password Rest Link
                    </button>
                </form>
            </div>
        </>
    )
}
