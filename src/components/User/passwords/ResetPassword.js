import React, { Component, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { useForm } from "../../../hooks/useForm";
import { resetPasswordService } from "../../../utils/services/auth/authService";

export const ResetPassword = () => {

    const { resetPasswordToken } = useParams();
    const history = useHistory();
    const [error, setError] = useState('');
    const [message, setMessage] = useState('');
    const [inputValues, handleInputChange, resetForm] = useForm({
        newPassword: ''
    });

    const resetPassword = async e => {
        e.preventDefault();
        const { newPassword } = inputValues;

        try {
            const { data } = await resetPasswordService({ resetPasswordLink: resetPasswordToken, newPassword });
            setMessage(data.message);
            history.push('/signin');
            resetForm();
        }
        catch (err) {
            if (err.response) {
                setError(err.response.data.error)
            }
            console.log(err)
        }
    };

    return (
        <>
            <div className="container login_container">
                <h2 className="mt-5 mb-5">Reset your Password</h2>

                {message && (
                    <h4 className="alert alert-success">{message}</h4>
                )}
                {error && (
                    <h4 className="alert alert-danger">{error}</h4>
                )}

                <form onSubmit={resetPassword}>
                    <div className="form-group mt-5">
                        <input
                            type="password"
                            className="form-control"
                            placeholder="Your new password"
                            value={inputValues.newPassword}
                            name="newPassword"
                            onChange={handleInputChange}
                            autoFocus
                        />
                    </div>
                    <button
                        type="submit"
                        className="btn btn-raised btn-primary"
                    >
                        Reset Password
                    </button>
                </form>
            </div>
        </>
    )
}
