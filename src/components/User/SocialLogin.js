import React from 'react';
import GoogleLogin from "react-google-login";
import { useHistory } from 'react-router-dom';
import { authenticateService, socialLoginService } from '../../utils/services/auth/authService';
import googleLogo from '../../utils/helpers/assets/google.png';
import './signin/signin.css';
// hooks react redux
import { useDispatch } from 'react-redux';
import { signinAction } from '../../redux/actions/user.action';
import GitHubLogin from 'github-login';

export const SocialLogin = ({ buttonTitle }) => {
    const history = useHistory();
    const dispatch = useDispatch();

    const responseGoogle = async ({ profileObj }) => {
        const { googleId, name, email, imageUrl } = profileObj;

        let user = {
            password: googleId || '',
            name: name,
            email: email,
            photo_profile: imageUrl
        };

        const { data } = await socialLoginService(user);
        dispatch(signinAction(data))
        authenticateService(data, () => {
            history.push('/ui')
        });
    };

    const onSuccessGit = response => console.log(response);
    const onFailureGit = response => console.error(response);

    return (
        <div className="">
            <GoogleLogin
                clientId="575488301651-2fi6n75lt3e1dj6jua30kv2ojlp784dg.apps.googleusercontent.com"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                render={renderProps => (
                    <button
                        onClick={renderProps.onClick}
                        className="btn btn-raised google-button d-flex justify-content-center align-items-center"
                        disabled={renderProps.disabled}
                    >
                        <img src={googleLogo} alt="logo" style={{ height: '30px', width: '30px' }} />
                        <span className="pl-2">{buttonTitle}</span>
                    </button>
                )}
            />

            {/* <GitHubLogin
                clientId="ac56fad434a3a3c1561e"
                onSuccess={onSuccessGit}
                onFailure={onFailureGit}
                render={renderProps => (
                    <button
                        onClick={renderProps.onClick}
                        className="btn btn-raised google-button d-flex justify-content-center align-items-center"
                        disabled={renderProps.disabled}
                    >
                        <span className="pl-2">Login with GitHub</span>
                    </button>
                )}
            /> */}
        </div>
    )
}
