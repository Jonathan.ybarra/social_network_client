import React, { useEffect, useState } from 'react';
import { UserItem } from './UserItem';
import { allUsersService } from '../../utils/services/user/UserService';
import { MinimalSpinner } from '../UI/MinimalSpinner/MinimalSpinner';
import './users.css';
import { isAuthenticated } from '../../utils/services/auth/authService';
import { Link } from 'react-router-dom';

export const Users = () => {
    const [usersState, setUsersState] = useState([]);
    const [loading, setLoading] = useState(false);
    const { user: userAuthor } = isAuthenticated();

    useEffect(() => {
        getUsers();
        return () => {
            setUsersState([])
        }
    }, []);

    const getUsers = async () => {
        setLoading(true);
        try {
            const { data } = await allUsersService();
            setUsersState(data.users);
        }
        catch (e) {
            console.log(e)
        }
        finally {
            setLoading(false);
        }
    }

    return (
        <>
            <div className="users-scroll bg-white position-fixed">

                <center className="my-4">
                    Users List
                </center>
                <center>
                    <Link to={'/ui/chat'} class="btn btn-info">Go to group chat</Link>
                </center>

                {
                    loading &&
                    <center className="mt-2">
                        <MinimalSpinner />
                    </center>
                }

                {
                    !loading &&
                    isAuthenticated() &&
                    <ul className="list-group">
                        {
                            usersState.length > 1 ?
                                usersState.map(user =>
                                    userAuthor._id != user._id &&
                                    <UserItem
                                        key={user._id}
                                        user={user}
                                        getUsers={getUsers}
                                    />
                                )
                                :
                                <center className="border p-3 text-secondary">
                                    You are the first user to register!
                                </center>
                        }
                    </ul>
                }
            </div>
        </>
    )
}

