import React from 'react'

export const EditProfileForm = ({ inputValues, handleInputChange }) => {
    return (
        <form>
            <h6 className="heading-small text-muted mb-4">User information</h6>
            <div className="pl-lg-4">
                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group focused">
                            <label className="form-control-label" htmlFor="input-username">Username</label>
                            <input
                                autoComplete="off"
                                name="name"
                                type="text"
                                className="form-control form-control-alternative"
                                placeholder="Username"
                                value={inputValues.name || ""}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label className="form-control-label" htmlFor="input-email">Email address</label>
                            <input
                                autoComplete="off"
                                name="email"
                                type="Email"
                                className="form-control form-control-alternative"
                                placeholder="jesse@example.com"
                                value={inputValues.email || ""}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                </div>
                {/* <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group focused">
                            <label className="form-control-label" htmlFor="input-new-password">New password</label>
                            <input
                                autoComplete="off"
                                name="password"
                                type="password"
                                className="form-control form-control-alternative"
                                placeholder="pass"
                                value={inputValues.password || ""}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                    <div className="col-lg-6">
                        <div className="form-group focused">
                            <label className="form-control-label" htmlFor="input-current-password">Current Password</label>
                            <input type="text" id="input-last-name" className="form-control form-control-alternative" placeholder="Current Password" />
                        </div>
                    </div>
                </div> */}
            </div>
            <hr className="my-4" />
            <h6 className="heading-small text-muted mb-4">About Me</h6>
            <div className="pl-lg-4">
                <div className="row">
                    <div className="col-lg-4">
                        <div className="form-group focused">
                            <label className="form-control-label" htmlFor="input-link-job">Link Profile Social</label>
                            <input
                                type="text"
                                id="input-link-job"
                                name="link_profile_job"
                                className="form-control form-control-alternative"
                                placeholder="Add link profile social ej.. Linkdedin - Git hub / lab"
                                value={inputValues.link_profile_job}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="form-group focused">
                            <label className="form-control-label" htmlFor="input-preference-one">Preferences 1</label>
                            <input
                                type="text"
                                id="input-preference-one"
                                className="form-control form-control-alternative"
                                placeholder="Insert your preference academic"
                                name="preference_one"
                                value={inputValues.preference_one}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                    <div className="col-lg-4">
                        <div className="form-group">
                            <label className="form-control-label" htmlFor="input-preference-two">Preferences 2</label>
                            <input
                                type="text"
                                id="input-preference-two"
                                name="preference_two"
                                className="form-control form-control-alternative"
                                placeholder="Insert your preference academic"
                                value={inputValues.preference_two}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                    <div className="col-lg-12 pl-lg-3 mt-2">
                        <div className="form-group">
                            <label>About Me</label>
                            <textarea
                                rows="5"
                                className="form-control form-control-alternative"
                                name="about"
                                placeholder="A few words about you ..."
                                value={inputValues.about || ""}
                                onChange={handleInputChange}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </form>
    )
}