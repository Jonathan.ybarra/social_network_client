import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { signoutService } from '../../../utils/services/auth/authService';
import { deleteUserService } from '../../../utils/services/user/UserService';

export const DeleteUser = ({ userId }) => {

    const [redirect, setRedirect] = useState(false);

    const deleteUserAccount = async () => {
        const { data } = await deleteUserService(userId);
        console.log(data);
        await signoutService();
        console.log('User is deleted!');
        setRedirect(true);
    }

    return (
        <>
            <button
                className="btn btn-sm btn-outline-danger float-right mx-3 mb-5"
                data-toggle="modal"
                data-target="#ModalDelete"
                type="button"
            >
                Delete account permanently
            </button>

            <div className="modal fade" id="ModalDelete" tabIndex="-1" role="dialog" aria-labelledby="ModalDeleteTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">Are you sure you want to delete your account?</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            ...
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button
                                className="btn btn-danger"
                                data-dismiss="modal"
                                type="button"
                                onClick={() => deleteUserAccount()}
                            >
                                Delete
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            {
                redirect && <Redirect to='/signin'/>
            }
        </>
    )
}
