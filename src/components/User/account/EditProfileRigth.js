import React, { useEffect, useState } from 'react';
import { signinAction } from '../../../redux/actions/user.action';
import { convertBase64 } from '../../../utils/helpers/ConvertImageToBase64';
import { defaultProfilePhoto } from '../../../utils/helpers/default_profile_photo';
import { setUserAuthenticateLocalStorage } from '../../../utils/services/auth/authService';
// hooks react redux
import { useDispatch, useSelector } from 'react-redux';
import { updateUserService } from '../../../utils/services/user/UserService';
// import { Loading } from '../../UI/Loading/Loading';
import './editProfile.css';
import { getPostByUserAction } from '../../../redux/actions/post.action';

export const EditProfileRigth = ({ user, getUser }) => {

    const [error, setError] = useState(null);
    const postsByUsersId = useSelector(store => store.posts);
    const dispatch = useDispatch();

    useEffect(() => {
        uploadImage();
        dispatch(getPostByUserAction(user._id))
        console.log(postsByUsersId)
    }, []);

    const uploadImage = () => {
        var uploader = document.createElement('input'),
            image = document.getElementById('img-result');

        uploader.type = 'file';
        uploader.accept = 'image/*';

        image.onclick = function () {
            uploader.click()
        }
        uploader.addEventListener('input', async ({ target }) => {
            try {
                console.log(target.files[0].size);
                if (target.files[0].size > 92000) {
                    setError("File size should be less than 92kb");
                }
                else {
                    const filteTo64 = await convertBase64(target.files[0]);
                    await updateUserService(user._id, { photo_profile: filteTo64 });
                    image.style.backgroundImage = `url(${filteTo64})`;
                    const newUser = setUserAuthenticateLocalStorage({ photo_profile: filteTo64 });
                    dispatch(signinAction(newUser))
                    setError(null);
                }
            } catch (e) {
                console.log(e)
            }
        });
    }

    return (
        <>
            <div className="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                <div className="card">
                    <center className="mt-3">
                        <button
                            className="no-image"
                            id="img-result"
                            style={{ backgroundImage: `url(${user.photo_profile ? user.photo_profile : defaultProfilePhoto})` }}
                        >
                            Upload Image
                        </button>
                        {
                            error &&
                            <p className="text-danger bg-light">{error}</p>
                        }
                    </center>

                    <div className="card-body pt-0 pt-md-4">
                        <div className="row">
                            <div className="col">
                                <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                                    <div className="mx-2">
                                        {
                                            user.followers &&
                                            <>
                                                <span className="heading">{user.followers.length}</span>
                                                <span className="description"> Followers</span>
                                            </>
                                        }
                                    </div>
                                    <div className="mx-2">
                                        {
                                            user.following &&
                                            <>
                                                <span className="heading">{user.following.length}</span>
                                                <span className="description"> Following</span>
                                            </>
                                        }
                                    </div>
                                    <div className="mx-2">
                                        <span className="heading">{postsByUsersId.totalItems || 0}</span>
                                        <span className="description"> Post</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="text-center">
                            <h3>
                                {user.name}
                                {/* <span className="font-weight-light">, 27</span> */}
                            </h3>
                            {/* <div className="h5 font-weight-300">
                                <i className="ni location_pin mr-2"></i>Bucharest, Romania
                                </div>
                            <div className="h5 mt-4">
                                <i className="ni business_briefcase-24 mr-2"></i>Solution Manager - Creative Tim Officer
                                </div>*/}
                            <div>
                                {user.email}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
