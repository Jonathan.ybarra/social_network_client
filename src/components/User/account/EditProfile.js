import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { useForm } from '../../../hooks/useForm';
import { userByIDService, updateUserService, updateUserLocalStorageService } from '../../../utils/services/user/UserService';
import { Loading } from '../../UI/Loading/Loading';
import { DeleteUser } from './DeleteUser';
import { EditProfileForm } from './EditProfileForm';
import { EditProfileRigth } from './EditProfileRigth';

export const EditProfile = () => {

    const history = useHistory();
    const { id: userID } = useParams();
    const [loading, setLoading] = useState(false);
    const [userState, setUserState] = useState({});
    const [error, setError] = useState(null);
    const [inputValues, handleInputChange, resetForm, setInputValues] = useForm({
        name: '',
        email: '',
        link_profile_job: '',
        preference_one: '',
        preference_two: '',
        password: '',
        about: ''
    });

    useEffect(() => {
        getUser();
    }, []);

    const getUser = async () => {
        setLoading(true);
        try {
            const { data } = await userByIDService(userID);
            setInputValues({
                name: data.name,
                email: data.email,
                link_profile_job: data.link_profile_job,
                preference_one: data.preference_one,
                preference_two: data.preference_two,
                about: data.about
            });
            setUserState(data);
            setLoading(false);
        }
        catch ({ response }) {
            if (response) {
                console.log(response.data.error);
            }
            else {
                console.log('Server down')
            }
            setLoading(false);
        }
    }

    const onUpdateUser = async () => {
        try {
            if (isValid()) {
                const { data } = await updateUserService(userID, { ...inputValues });
                updateUserLocalStorageService(data.user, () => {
                    history.push(`/ui/user/${userID}`);
                });
            }
        }
        catch ({ response }) {
            if (response) {
                setError(response.data.error);
            }
            else {
                setError('Server down')
            }
            setLoading(false);
        }
    }

    const isValid = () => {
        const { name, email, password } = inputValues;
        if (name.length === 0) {
            setError('Name is Required');
            return false;
        }
        if (email.length === 0) {
            setError('Email is Required');
            return false;
        }
        // if (name.length === 0) setError('Name is Required')
        return true;
    }

    return (
        <>
            {
                loading && <Loading />
            }
            {
                !loading &&
                <div className="container-fluid mt-5 pt-5">
                    {
                        error &&
                        <div className="alert alert-danger alert-dismissible fade show mt-5 z-index" role="alert">
                            {error}
                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    }
                    <div className="row">
                        <EditProfileRigth
                            user={userState}
                            setLoading={setLoading}
                            getUser={getUser}
                        />
                        <div className="col-xl-8 order-xl-1">
                            <div className="card shadow">
                                <div className="card-header border-0">
                                    <div className="row align-items-center">
                                        <div className="col-8">
                                            <h3 className="mb-0">My account</h3>
                                        </div>
                                        <div className="col-4 text-right">
                                            <button
                                                className="btn btn-sm btn-primary"
                                                onClick={() => onUpdateUser()}
                                            >
                                                Update User
                                        </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <EditProfileForm
                                        inputValues={inputValues}
                                        handleInputChange={handleInputChange}
                                        onUpdateUser={onUpdateUser}
                                    />
                                </div>

                                <div className="justify-content-end">
                                    <DeleteUser userId={userState._id} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>

    )
}

    // <div>
    //     {
    //         loading && <Loading />
    //     }
    //     {
    //         !loading &&
    //         <div>
    //             <EditProfileForm
    //                 inputValues={inputValues}
    //                 handleInputChange={handleInputChange}
    //                 onUpdateUser={onUpdateUser}
    //             />
    //         </div>
    //     }

    //     {
    //         !error && 
    //         <button type="button" className="btn btn-secondary" data-toggle="snackbar" data-style="toast" data-content="Fried chicken out of stock.">
    //         Toast
    //       </button>
    //     }
    // </div>