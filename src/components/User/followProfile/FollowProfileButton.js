import React from 'react';
import { followUserService, unfollowUserService } from '../../../utils/services/user/FollowUnFollow';
import './follow.css';
export const FollowProfileButton = ({ following, onclickFollowButton }) => {

    const followButton = () => {
        onclickFollowButton(followUserService);
    }

    const unfollowButton = () => {
        onclickFollowButton(unfollowUserService);
    }

    return (
        <>
            {
                following ?
                    <button
                        className="btn-unfollow"
                        onClick={() => unfollowButton()}
                    >
                        UnFollow
                    </button>
                    :
                    <button
                        className="btn-follow"
                        onClick={() => followButton()}
                    >
                        Follow
                    </button>
            }
        </>
    )
}
