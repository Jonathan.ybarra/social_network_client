import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { userByIDService } from '../../../utils/services/user/UserService';
import { MinimalSpinner } from '../../UI/MinimalSpinner/MinimalSpinner';
import { FollowItem } from './FollowItem';

export const ProfileFollowing = () => {

    const { id: userID } = useParams();
    const [loading, setLoading] = useState(false);
    const [userState, setUserState] = useState({
        following: [],
        followers: []
    });

    useEffect(() => {
        userId(userID);
    }, [userID]);

    const userId = async (id) => {
        setLoading(true);
        try {
            const { data } = await userByIDService(id);
            setUserState(data);
            setLoading(false);
        }
        catch ({ response }) {
            if (response) {
                console.log(response.data.error);
            }
            else {
                console.log('Server down')
            }
            setLoading(false);
        }
    }

    return (
        <>
            {
                loading &&
                <center className="mt-3">
                    <MinimalSpinner />
                </center>
            }

            {
                userState.following.length > 0 &&
                !loading &&
                <div className="table-responsive">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Photo Profile</th>
                                <th scope="col">Username</th>
                                <th scope="col">Email</th>
                                <th scope="col">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                userState.following.map((person, index) =>
                                    <FollowItem
                                        user={person}
                                        key={person._id}
                                        index={index}
                                        userID={userId}
                                    />
                                )
                            }
                        </tbody>
                    </table>
                </div>
            }

            {
                userState.following.length === 0 &&
                !loading &&
                <center className="my-5">you still don't follow anyone</center>

            }
        </>
    )
}
