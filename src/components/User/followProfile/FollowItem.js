import React, { useState } from 'react';
import { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { defaultProfilePhoto } from '../../../utils/helpers/default_profile_photo';
import { checkFollow, clickFollowButton } from '../../../utils/helpers/FollowHelper';
import { isAuthenticated } from '../../../utils/services/auth/authService';
import { followUserService, unfollowUserService } from '../../../utils/services/user/FollowUnFollow';
import { userByIDService } from '../../../utils/services/user/UserService';

export const FollowItem = ({ user, index, userID, deleteFollower, isAuthor }) => {

    const [following, setFollowing] = useState(false);
    const [userState, setUserState] = useState({});


    useEffect(() => {
        userId(user._id)
    }, [user]);

    const userId = async (id) => {
        try {
            const { data } = await userByIDService(id);
            setUserState(data);
            setFollowing(checkFollow(data))
        }
        catch ({ response }) {
            if (response) {
                console.log(response.data.error);
            }
            else {
                console.log('Server down')
            }
        }
    }

    const followButton = async () => {
        await clickFollowButton(followUserService, setFollowing, following, user._id);
        userID();
    }

    const unFollowButton = async () => {
        await clickFollowButton(unfollowUserService, setFollowing, following, user._id);
        userID();
    }

    return (
        <>
            <tr

            >
                <th scope="row" className="pt-4">{index + 1}</th>
                <td>
                    <th scope="col">
                        <Link to={`/ui/user/${user._id}`}>
                            <img
                                alt={user._id}
                                style={{ width: '2em', height: '2em', borderRadius: '2em' }}
                                src={user.photo_profile ? user.photo_profile : defaultProfilePhoto}
                            />
                        </Link>
                    </th>
                </td>
                <td className="pt-4">
                    <Link
                        to={`/ui/user/${user._id}`}
                        className="name_table_link"
                    >
                        {user.name}
                    </Link>
                </td>
                <td className="pt-4">{user.email}</td>
                <td className="pt-3">
                    {
                        isAuthenticated() &&
                            user._id === isAuthenticated().user._id ?
                            <Link
                                to={`/ui/user/${user._id}`}
                                className="btn btn-info btn-sm"
                            >
                                Me
                            </Link> :
                            following ?
                                <span
                                    className="btn btn-danger btn-sm"
                                    onClick={() => unFollowButton()}

                                >
                                    Un follow
                                    </span> :
                                <span
                                    className="btn btn-info btn-sm"
                                    onClick={() => followButton()}
                                >
                                    Follow
                                </span>
                    }
                </td>
                <td>
                    {
                        isAuthor &&
                        <span
                            className="btn btn-danger btn-sm"
                            data-toggle="modal"
                            data-target="#modalDeleteFollow"
                        >
                            Delete
                    </span>
                    }
                </td>
            </tr>

            {/* Modal */}
            <div className="modal fade" id="modalDeleteFollow" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="d-block">
                            <button type="button" className="btn btn-block btn-secondary" data-dismiss="modal">Cancel</button>
                            <button
                                type="button"
                                className="btn btn-block btn-danger"
                                data-dismiss="modal"
                                onClick={() => deleteFollower(user._id)}
                            >
                                Confirm Delete follower?
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
