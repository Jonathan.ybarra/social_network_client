import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { isAuthenticated } from '../../../utils/services/auth/authService';
import { deleteFollowerService, unfollowUserService } from '../../../utils/services/user/FollowUnFollow';
import { userByIDService } from '../../../utils/services/user/UserService';
import { Loading } from '../../UI/Loading/Loading';
import { MinimalSpinner } from '../../UI/MinimalSpinner/MinimalSpinner';
import { FollowItem } from './FollowItem';

export const ProfileFollowers = () => {

    const { id: userID } = useParams();
    const [loading, setLoading] = useState(false);
    const [isAuthor, setIsAuthor] = useState(false);
    const [userState, setUserState] = useState({
        following: [],
        followers: []
    });

    useEffect(() => {
        userId();
    }, [userID]);

    const userId = async () => {
        setLoading(true);
        try {
            const { data } = await userByIDService(userID);
            setUserState(data);
            console.log(data)
        }
        catch ({ response }) {
            if (response) {
                console.log(response.data.error);
            }
            else {
                console.log('Server down')
            }
        }
        finally {
            setLoading(false);
        }
    }

    const deleteFollower = async (followId) => {
        const { user } = isAuthenticated();
        const res = await deleteFollowerService(user._id, followId);
        console.log(res)
        userId();

    }

    return (
        <>
            {
                loading &&
                <center className="mt-3">
                    <MinimalSpinner />
                </center>
            }
            {
                userState.followers.length > 0 &&
                !loading &&
                <div className="table-responsive">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Photo Profile</th>
                                <th scope="col">Username</th>
                                <th scope="col">Email</th>
                                <th scope="col">Option</th>
                                {
                                    isAuthenticated().user &&
                                    isAuthenticated().user._id === userState._id &&
                                    <th scope="col">Delete follower</th>
                                }
                            </tr>
                        </thead>
                        <tbody>
                            {
                                isAuthenticated() &&
                                userState.followers.map((person, index) =>
                                    <FollowItem
                                        user={person}
                                        key={person._id}
                                        index={index}
                                        userID={userId}
                                        deleteFollower={deleteFollower}
                                        isAuthor={isAuthenticated().user._id === userState._id}
                                    />
                                )
                            }
                        </tbody>
                    </table>
                </div>
            }
            {
                userState.followers.length === 0 &&
                !loading &&
                <center className="my-5">has no followers yet</center>
            }
        </>
    )
}
