import React from 'react';
import { Link } from 'react-router-dom';
import { SocialLogin } from '../SocialLogin';

export const FormSignin = ({ onSignin, inputValues, handleInputChange, recaptcha, recaptchaHandler, handleCheckedChange }) => {
    return (
        <form onSubmit={onSignin}>
            <div className="form-group">
                <label htmlFor="" className="bmd-label-floating">Email</label>
                <input
                    autoComplete="off"
                    name="email"
                    type="Email"
                    className="form-control"
                    value={inputValues.email}
                    onChange={handleInputChange}
                />
            </div>

            <div className="form-group">
                <label htmlFor="" className="bmd-label-floating">Password</label>
                <input
                    autoComplete="off"
                    name="password"
                    type="password"
                    className="form-control"
                    value={inputValues.password}
                    onChange={handleInputChange}
                />
            </div>
            <div className="checkbox mt-4">
                <label style={{ fontSize: '14px' }}>
                    <input
                        type="checkbox"
                        name='rememberUser'
                        onChange={handleCheckedChange}
                        checked={inputValues.rememberUser}
                    />
                    Remember me
                </label>
            </div>
            <button type="submit" className="btn btn-raised btn-block btn-primary login">Login</button>

            {/* <div className="d-flex justify-content-center align-items-center mt-5 mb-3">
                <span className="line"></span> <small className="px-2 line-text">OR</small> <span className="line"></span>
            </div> */}

            <center className="mt-3">
                <SocialLogin buttonTitle={"Login with google"} />
            </center>

            <div className="row mt-4">
                <div className="col-md-6 col-sm-12">
                    <Link
                        to="/forgot-password"
                        className="btn mt-3"
                        style={{ color: '#6ca0d6', fontWeight: '400', textTransform: 'capitalize' }}
                    >
                        Forgot Password
                    </Link>
                </div>
                <div className="col-md-6 col-sm-12">
                    <div className="form-group">
                        <label className="text-muted">
                            {recaptcha ? "Thanks. You got it!" : "What day is today?"}
                        </label>

                        <input
                            onChange={recaptchaHandler}
                            type="text"
                            className="form-control"
                        />
                    </div>
                </div>
            </div>


            <div className="p-3 d-flex flex-row justify-content-center align-items-center member border-top">
                <span>Not a member? </span> <Link to="/signup" className="text-decoration-none ml-2">SIGNUP</Link>
            </div>
        </form >
    )
}
