import React, { useEffect, useState } from 'react';
import { useForm } from '../../../hooks/useForm';
import { FormSignin } from './FormSignin';
import { Loading } from '../../UI/Loading/Loading';
import { recaptchaHelper } from '../../../utils/helpers/recaptchaHelper';
import { signinService, authenticateService } from '../../../utils/services/auth/authService';
import { ToastContainer, toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';
import './signin.css';

export const Signin = () => {

    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const [recaptcha, setRecaptcha] = useState(false);
    const [redirectToReferer, setRedirectToReferer] = useState(false);
    const [inputValues, handleInputChange, resetForm, setStateValues, onEmojiClick, handleInputChangeFile, handleCheckedChange] = useForm({
        email: '',
        password: '',
        rememberUser: false
    });

    useEffect(() => {
        const user = JSON.parse(localStorage.getItem('user_password_remember'));
        if (user) {
            setStateValues({
                email: user.email,
                password: user.password,
                rememberUser: true
            })
        }
    }, [])

    const onSignin = async (e) => {
        e.preventDefault();
        setLoading(true);
        const user = {
            email: inputValues.email,
            password: inputValues.password
        }
   
        try {
            if (recaptcha) {
                const { data } = await signinService({ ...user });
                authenticateService(data, () => {
                    setRedirectToReferer(true)
                })
                if (typeof window !== "undefined" && inputValues.rememberUser) {
                    localStorage.setItem('user_password_remember', JSON.stringify(user));
                }
                else {
                    localStorage.removeItem('user_password_remember');
                }
                setError(null);
            }
            else {
                toast.error("What day is today? Please write a correct answer!", {
                    draggablePercent: 60,
                    position: toast.POSITION.BOTTOM_RIGHT
                });
            }
        }
        catch ({ response }) {
            if (response.data) {
                setError(response.data.error);
            }
            else {
                console.log(response)
            }
        }
        finally {
            setLoading(false);
        }
    }

    const recaptchaHandler = ({ target }) => {
        const validRecaptcha = recaptchaHelper(target.value.toLowerCase());
        setRecaptcha(validRecaptcha)
    }

    return (
        <>
            {
                loading ?
                    <Loading /> :
                    <div className="container">
                        {
                            error &&
                            <div className="alert alert-danger alert-dismissible fade show mt-5 " role="alert">
                                {error}
                                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        }

                        <div className="row">
                            <div className="col-12 col-md-6 login_container">
                                <center>
                                    <header className="my-4">Log in with your email account</header>
                                </center>
                                <FormSignin
                                    onSignin={onSignin}
                                    inputValues={inputValues}
                                    handleInputChange={handleInputChange}
                                    recaptcha={recaptcha}
                                    recaptchaHandler={recaptchaHandler}
                                    handleCheckedChange={handleCheckedChange}
                                />
                            </div>
                        </div>

                        <ToastContainer draggablePercent={60} />
                    </div>
            }

            {
                redirectToReferer && <Redirect to={'/'} />
            }

        </>
    )
}
