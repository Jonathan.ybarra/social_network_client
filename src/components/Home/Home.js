import React, { useState } from 'react';
import { Switch, useHistory } from 'react-router-dom';
import { RouteWithSubRoutes } from '../../routes/RouteWithSubRoutes';

import { Navbar } from '../UI/Navbar/Navbar';
import { PostFollowsMe } from './PostFollowsMe';

export const Home = ({ routes }) => {

    const { location } = useHistory();

    return (
        <>
            <Navbar />
            {
                location.pathname === '/ui' &&
               <PostFollowsMe/>
            }

            <div>
                <Switch>
                    {
                        routes &&
                        routes.map((route, i) => (
                            <RouteWithSubRoutes key={i} {...route} />
                        ))
                    }
                </Switch>
            </div>
        </>
    )
}
