import React, { useState, useEffect } from 'react';
import { PostItem } from '../Post/postItem/PostItem';
import { MinimalSpinner } from '../UI/MinimalSpinner/MinimalSpinner';
import InfiniteScroll from 'react-infinite-scroll-component';
import { deletePostIdService, getPostUserMeService } from '../../utils/services/post/postService';
import { isAuthenticated } from '../../utils/services/auth/authService';
import { PostItemModalDelete } from '../Post/postItem/PostItemModalDelete';
// hooks react redux
import { useDispatch, useSelector } from 'react-redux';
import { deletePostAction, getPostByUserAction } from '../../redux/actions/post.action';
import { Users } from '../User/Users';

export const PostFollowsMe = () => {
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(0);
    const [postState, setPostState] = useState([]);
    const [loading, setLoading] = useState(false);
    const [postDeleteState, setPostDeleteState] = useState(null);
    const dispatch = useDispatch();
    const { user } = isAuthenticated();

    useEffect(() => {
        setLoading(true);
        getPostFollowingsAndme();
        setLoading(false);
        return () => {
            setPostState([])
        }
    }, []);

    const deletePost = async () => {
        setLoading(true);
        await deletePostIdService(postDeleteState._id);
        getPostFollowingsAndme();
        setLoading(false);
    }

    const getPostFollowingsAndme = async () => {
        try {
            const { data } = await getPostUserMeService(user._id, page);
            setPostState(postState.concat(data.posts));
            setTotalPages(Math.ceil(data.totalItems / 6))
            if (page <= Math.ceil(data.totalItems / 6)) {
                setPage(page + 1);
            }
        }
        catch (e) {
            console.log(e)
        }
    }
    return (
        <>
            {
                loading &&
                <center className="pt-5" style={{ position: 'relative' }}>
                    <MinimalSpinner />
                </center>
            }

            {
                !loading &&
                <div className="pl-5 pt-95">
                    <div className="row">
                        <div className="col-md-8">
                            <InfiniteScroll
                                dataLength={postState.length}
                                next={getPostFollowingsAndme}
                                hasMore={page <= totalPages}
                                loader={
                                    <center className="pt-5">
                                        <MinimalSpinner />
                                    </center>
                                }
                                style={{ overflowY: "hidden" }}
                            >
                                {
                                    postState.map(post =>
                                        <PostItem
                                            key={post._id}
                                            post={post}
                                            setPostDeleteState={setPostDeleteState}
                                        />
                                    )
                                }
                            </InfiniteScroll>
                            <PostItemModalDelete
                                deletePost={deletePost}
                            />
                        </div>
                        <div className="col-md-4">
                            <Users />
                        </div>
                    </div>
                </div>
            }
        </>
    )
}
