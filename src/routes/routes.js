import React from 'react';
import { Redirect } from "react-router-dom";
import { ChatScreen } from '../components/Chat/ChatScreen';
import { Home } from "../components/Home/Home";
import { EditPost } from '../components/Post/singlePost/EditPost';
import { PostScreen } from '../components/Post/PostScreen';
import { SinglePost } from '../components/Post/singlePost/SinglePost';
import { EditProfile } from '../components/User/account/EditProfile';
import { ProfileFollowers } from '../components/User/followProfile/ProfileFollowers';
import { ProfileFollowing } from '../components/User/followProfile/ProfileFollowing';
import { ForgotPassword } from '../components/User/passwords/ForgotPassword';
import { ResetPassword } from '../components/User/passwords/ResetPassword';
import { ProfileUser } from "../components/User/profile/ProfileUser";
import { Signin } from "../components/User/signin/Signin";
import { Signup } from "../components/User/signup/Signup";
import { Users } from '../components/User/Users';

const routes = [
    {
        path: "/ui",
        restricted: true,
        component: Home,
        routes: [
            {
                path: "/ui/user/:id",
                restricted: false,
                component: ProfileUser,
                routes: [
                    {
                        path: "/ui/user/:id/followers",
                        restricted: false,
                        component: ProfileFollowers
                    },
                    {
                        path: "/ui/user/:id/followings",
                        restricted: false,
                        component: ProfileFollowing
                    },
                    {
                        path: "/ui/user/:id/posts",
                        restricted: false,
                        component: PostScreen
                    },


                ]
            },
            {
                path: "/ui/edit-user/:id",
                restricted: true,
                component: EditProfile
            },
            {
                path: "/ui/users",
                restricted: false,
                component: Users
            },
            {
                path: "/ui/post/:postId",
                restricted: false,
                component: SinglePost
            },
            {
                path: "/ui/edit-post/:postId",
                restricted: false,
                component: EditPost
            },
            {
                path: "/ui/chat",
                restricted: false,
                component: ChatScreen
            },
            {
                component: () => <Redirect to="/ui" />
            }
        ]
    },
    {
        path: "/forgot-password",
        component: ForgotPassword
    },
    {
        path: "/reset-password/:resetPasswordToken",
        component: ResetPassword
    },
    {
        path: "/signup",
        component: Signup
    },
    {
        path: "/signin",
        component: Signin,
    },
    {
        component: () => <Redirect to="/ui" />
    }
];

export default routes;