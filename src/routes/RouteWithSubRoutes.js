import React from 'react';
import { Redirect, Route } from "react-router-dom";
import { isAuthenticated } from '../utils/services/auth/authService';

export const RouteWithSubRoutes = (route) => {
    console.log(route.restricted)
    return (
        <Route
            path={route.path}
            render={props => {
                if (!route.restricted) {
                    return <route.component {...props} routes={route.routes} />
                }
                if (route.restricted && !isAuthenticated()) {
                    return <Redirect to={{ pathname: '/signin', state: { from: props.location } }} />
                }
                else {
                    return <route.component {...props} routes={route.routes} />
                }

            }
            }
        />
    )
}
