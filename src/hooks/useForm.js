import { useState } from "react";
import { convertBase64 } from "../utils/helpers/ConvertImageToBase64";

export const useForm = (initialState = {}) => {
    const [values, setValues] = useState(initialState);

    const setStateValues = (newValues) => {
        setValues(newValues);
    }

    const reset = () => {
        setValues(initialState);
    }

    const handleInputChange = ({ target }) => {
        setValues({
            ...values,
            [target.name]: target.value
        });
    }

    const handleCheckedChange = ({ target }) => {
        setValues({
            ...values,
            [target.name]: target.checked
        });
    }

    const handleInputChangeFile = async ({ target }) => {
        const file = target.files[0];
        if (file) {
            const fileTo64 = await convertBase64(file);
            setValues({
                ...values,
                [target.name]: fileTo64
            });
        }
    }
    const onEmojiClick = ({ target }, emojiObject) => {
        console.log(emojiObject)
        setValues({
            ...values,
            body: values.body + emojiObject.emoji
        });
    };

    return [values, handleInputChange, reset, setStateValues, onEmojiClick, handleInputChangeFile, handleCheckedChange];
}